# Tap Dance

Games for the Kantega wall project.

### Emulate

```sh
sudo dnf install python3 python3-pygame
```

```sh
python3 src/emulate.py
```

### Run

```sh
pip install adafruit-circuitpython-busdevice
pip install adafruit-circuitpython-neopixel
pip install adafruit-circuitpython-mcp230xx
```

```sh
sudo python3 src/run.py
```


### TODO

- Command line arguments
- Sound effects
